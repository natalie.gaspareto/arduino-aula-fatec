int botao = 3;
int led_down = 12;
int estado = 0;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(botao, INPUT_PULLUP);
  pinMode(led_down, OUTPUT);
  digitalWrite(led_down, LOW);
  
}

void loop() {
  // reply only when you receive data:

  estado = digitalRead(botao);
  Serial.print("Estado= ");
  Serial.println(estado);
  delay(200);

  if(estado==0){

    digitalWrite(led_down, LOW);
    
  }

  else {

    digitalWrite(led_down, HIGH);
    
  }
  
}
