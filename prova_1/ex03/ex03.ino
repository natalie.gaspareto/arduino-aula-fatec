int led0 = 8;
int led1 = 9;
int led2 = 10;
int bt_up = 3;
int bt_down = 2;

int acum1 = 0;
int acum2 = 0;

int potenc = A0;
int valor_potenc;

void setup() {
  Serial.begin(9600);
  pinMode(led0, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(bt_up, INPUT);
  pinMode(bt_down, INPUT);
  pinMode(potenc, INPUT);
}

void loop() {
  int estado_up = digitalRead(bt_up);
  Serial.print("Estado up = ");
  Serial.println(estado_up);
  delay(1000);
  
  //int estado_down = digitalRead(bt_down);
  //Serial.print("Estado down = ");
  //Serial.println(estado_down);

  if (estado_up == 0) {
    for (int i = 0; i < 10; i ++) {
      
      valor_potenc = analogRead(potenc); 
      Serial.print("Potenciometro = ");
      Serial.println(valor_potenc);
      
      delay(1000);
      Serial.print("i = ");
      Serial.println(i);
      
      if (valor_potenc % 2 == 0) { 
        acum1 += valor_potenc; 
        
        
      } else {
        acum2 += valor_potenc;
        
      }
    }

    Serial.print("Soma Par: ");
    Serial.println(acum1);
      
    Serial.print("Soma Impar: ");
    Serial.println(acum2);
  }
}
