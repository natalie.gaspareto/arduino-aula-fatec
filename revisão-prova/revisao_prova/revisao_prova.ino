int red = 3;
int yellow = 5;
int green = 6;
int white = 9;
int bt_up = 2;
int bt_down = 4;
int analogPin = A5; // pino para leitura do potenciômetro
int valor = 0;
int acum1 = 0 , acum2 = 0, cont1 = 0, cont2 = 0;
float media = 0;

void setup(){
  Serial.begin(9600);
  
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(white, OUTPUT);
  pinMode(bt_up, INPUT);
  pinMode(bt_down, INPUT);
}

void loop() {
  int estado_up = digitalRead(bt_up);
  Serial.print("Estado UP: ");
  Serial.println(estado_up);
  delay(100);

  int estado_down = digitalRead(bt_down);
  Serial.print("Estado Down: ");
  Serial.println(estado_down);
  delay(100);

  valor = analogRead(analogPin); 
  analogWrite(white, valor / 4);
  Serial.print("Potenciomentro: ");
  Serial.println(valor);
  delay(200);


  if(estado_down == 0) {
    for (int i = 0; i < 10; i ++) {
    	valor = analogRead(analogPin); 
	delay(100);
  	Serial.print("i = ");
	Serial.println(i);
			
      	if(valor % 2 == 0) {
	  acum1 += valor;
	  cont1 ++;   
	}
      }
    Serial.print("Media Pares = ");
    Serial.println(media = acum1/cont1);
    acum1 = 0;
    cont1 = 0;
    delay(1000);
  }
  
  if(estado_up == 1) {
    for (int j = 0; j < 10; j ++) {
      valor = analogRead(analogPin); 
      delay(100);
      Serial.print("j = ");
      Serial.println(j);

      if(valor % 2 == 1) {
	acum2 += valor;
	cont2 ++;   
      }
    }
    Serial.print("Media Impares = ");
    Serial.println( media = acum2 / cont2);
    acum2 = 0;
    cont2 = 0;
    delay(1000);
  }

  if (media <= 200) {
    digitalWrite(green,1);
    digitalWrite(red, 0);
    digitalWrite(yellow, 0);
  }
  else if (media <= 400) {
    digitalWrite(yellow,1);
    digitalWrite(green, 0);
    digitalWrite(red, 0);
  }
  else {
    digitalWrite(red,1);
    digitalWrite(yellow, 0);
    digitalWrite(green, 0);
  }    
}
