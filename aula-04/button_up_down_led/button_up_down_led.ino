int bt1 = 3;
int bt2 = 4;
int vermelho = 11;
int amarelo = 12;
int verde = 10;
int estado_down = 0;
int estado_up = 0;
int cont = 0;


void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(bt1, INPUT_PULLUP);
  pinMode(bt2, INPUT_PULLUP);
  pinMode(vermelho, OUTPUT);
  pinMode(amarelo, OUTPUT);
  pinMode(verde, OUTPUT);
  
}

void loop() {
  // reply only when you receive data:

  estado_down = digitalRead(bt1);
  Serial.print("Estado Down= ");
  Serial.println(estado_down);
  (200);

  estado_up = digitalRead(bt2);
  Serial.print("Estado Up= ");
  Serial.println(estado_up);
  delay(200);

  if(estado_down==0){

    cont--;
    
  }

  if (estado_up==0){
    
    cont++;

  }
  Serial.print("CONT= ");
  Serial.println(cont);

  delay (200);

  if(cont==1){

    digitalWrite(vermelho, HIGH);
    digitalWrite(amarelo, LOW);
    digitalWrite(verde, LOW);
    
  }

  else if(cont==2){

    digitalWrite(vermelho, LOW);
    digitalWrite(amarelo, HIGH);
    digitalWrite(verde, LOW);
    
  }

   else if(cont==3){

    digitalWrite(vermelho, LOW);
    digitalWrite(amarelo, LOW);
    digitalWrite(verde, HIGH);
    
  }

  else{

    digitalWrite(vermelho, LOW);
    digitalWrite(amarelo, LOW);
    digitalWrite(verde, LOW);
    
  }
  
}
