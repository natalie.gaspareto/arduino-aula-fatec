int pinoBotao = 3;
int pinoLed = 12;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pinoBotao, INPUT_PULLUP); 
  pinMode(pinoLed, OUTPUT);
  digitalWrite(pinoLed, HIGH);

}

void loop() {
  if(digitalRead(pinoBotao) == LOW) {
    digitalWrite(pinoLed, LOW);
    Serial.println("Botão não pressionado");
    delay(200);
  } else {
    digitalWrite(pinoLed, HIGH);
    Serial.println("Botão pressionado");
    delay(200);
  }
}
